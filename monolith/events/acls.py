from .keys import OPEN_WEATHER_API_KEY, PEXELS_API_KEY

import json
import requests


def get_location_picture(location_city, location_state):
    # Create a dictionary for the headers to use in the request
    headers = {"Authorization": PEXELS_API_KEY}

    # Create the URL for the request with the city and state
    # url = f"https://api.pexels.com/v1/search?query={location_city},{location_state}&per_page=1"
    url = "https://api.pexels.com/v1/search"
    params = {"query": f"{location_city} {location_state}", "per_page": 1}

    # Make the request
    response = requests.get(url, params=params, headers=headers)

    # Translate JSON response to Python dictionary
    location = json.loads(response.content)

    try:
        # Return a dictionary that contains a `picture_url` key and
        #   one of the URLs for one of the pictures in the response
        # location_picture = {"picture_url": location["photos"][0]["url"]}
        location_picture = {
            "picture_url": location["photos"][0]["src"]["original"]
        }

        # Return dict
        return location_picture
    except:
        return {"picture_url": None}


def get_weather_data(location_city, location_state):
    # Create the URL for the geocoding API with the city and state
    url = f"http://api.openweathermap.org/geo/1.0/direct?q={location_city},{location_state},US&limit=1&appid={OPEN_WEATHER_API_KEY}"

    # Make the request
    response_1 = requests.get(url)

    # Parse the JSON response
    geocoding = json.loads(response_1.content)

    # Get the latitude and longitude from the response
    latitude = geocoding[0]["lat"]
    longitude = geocoding[0]["lon"]

    # Create the URL for the current weather API with the latitude
    #   and longitude
    url = f"https://api.openweathermap.org/data/2.5/weather?lat={latitude}&lon={longitude}&appid={OPEN_WEATHER_API_KEY}"

    # Make the request
    response_2 = requests.get(url)

    # Parse the JSON response
    weather = json.loads(response_2.content)

    try:
        # Get the main temperature and the weather's description and put
        #   them in a dictionary
        temp_in_f = 1.8 * ((weather["main"]["temp"]) - 273) + 32

        weather_data = {
            "temp": f"{int(temp_in_f)} degrees Fahrenheit",
            "description": weather["weather"][0]["description"],
        }
        # Return the dictionary
        return weather_data
    except:
        return None
