import json
import requests

from .models import ConferenceVO

# Polling for all of the conferences from the url monolith:8000/api/conferences
def get_conferences():
    url = "http://monolith:8000/api/conferences/"
    response = requests.get(url)
    content = json.loads(response.content)

    # Iterates through each conference dictionary
    for conference in content["conferences"]:
        # Either creates or updates the conference with the name and href within attendees_microservice
        ConferenceVO.objects.update_or_create(
            import_href=conference["href"],
            defaults={"name": conference["name"]},
        )
